const fs = require('fs').promises;

const data = [];
let count = 0;

const main = async () => {
    const dir = await fs.readdir('./');

    for (let i = 0; i < dir.length; i++) {
        const stat = await fs.stat(`./${dir[i]}`);

        if (stat.isDirectory() === false) {
            continue;
        }

        const currentData = {
            imagePath: '',
            titlePath: '',
            descPath: '',
        };

        const inner = await fs.readdir(`./${dir[i]}`);
        const hasShuMing = inner.find(item => item.includes('《'));

        if (hasShuMing) {
            currentData.titlePath = `https://gitlab.com/phenomLi/li-mao-images/-/raw/main/jia-hua/${hasShuMing}`;
        } else {
            const titlePath = inner.find(item => !item.includes('_'));
            currentData.titlePath = `https://gitlab.com/phenomLi/li-mao-images/-/raw/main/jia-hua/${titlePath}`;
        }

        for (let j = 0; j < inner.length; j++) {
            const file = inner[j];

            if (hasShuMing) {
                if (file.includes('描述')) {
                    currentData.descPath = `https://gitlab.com/phenomLi/li-mao-images/-/raw/main/jia-hua/${file}`;
                } else {
                    if (!file.includes('《')) {
                        currentData.imagePath = `https://gitlab.com/phenomLi/li-mao-images/-/raw/main/jia-hua/${file}`;
                    }
                }
            } else {
                if (file.includes('02')) {
                    currentData.descPath = `https://gitlab.com/phenomLi/li-mao-images/-/raw/main/jia-hua/${file}`;
                }

                if (file.includes('03')) {
                    currentData.imagePath = `https://gitlab.com/phenomLi/li-mao-images/-/raw/main/jia-hua/${file}`;
                }
            }
        }

        data.push(currentData);
    }

    console.log(data);
};

main();

